use strict;
use warnings;
use Test::More;
use Fedora::VSP;
use version;

my @sample = qw(
5.8
v5.8
5.002
5.002
5.008
5.008008
5.005
5.008
5.006
5.006
5.005
5.007
v5.16
5.005_03
5.008003
5.008001
5.002
5.006_000
5.006001
5.006
5.008.004
5.008000
1
v1
1.
v1.
.
.1
.1.2
);

plan tests => 2 * scalar @sample;

sub compute_expected {
    my $input = shift;

    # We ignore parts after underscore
    $input =~ s/_.*//;
    my $reference = eval { version->parse($input)->normal };
    if (defined $reference) {
        # We strip leading "v"
        $reference =~ s/^v//;
    }

    return $reference;
}

sub compute_short_expected {
    my $reference = compute_expected($_[0], 1);

    if (defined $reference) {
        # Cut off all trailing zero groups
        $reference =~ s/(?:\.0*)*$//;
        # Drop 0 integer
        if ($reference =~ /^0+\.?$/) {
            $reference = undef;
        }
    }

    return $reference;
}

for my $input (@sample) {
    is (Fedora::VSP::vsp($input), compute_expected($input), "version $input");
}
for my $input (@sample) {
    is (Fedora::VSP::vsp($input, 1), compute_short_expected($input),
        "short $input");
}
